let $mintOpen = $("#mint-open");

chrome.tabs.query(
    {
        url: [
            "*://mint.intuit.com/*",
            "*://accounts.intuit.com/*"]
    }, (matches) => {
        if (matches.length > 0) {
            $mintOpen.on("click", () => {
                chrome.tabs.highlight({tabs: [matches[0].index]});
            });
            $mintOpen.html(`Jump to open Intuit&reg; Mint&trade; tab`);
        }else{
            $mintOpen.on("click", () => {
                chrome.tabs.create({
                   url: "https://mint.intuit.com/mintoil.event"
                }, (tab) =>{
                    chrome.tabs.highlight({tabs: [tab.index]});
                });
            });
            $mintOpen.html(`Open new tab to Intuit&reg; Mint&trade;`);
        }
    });

