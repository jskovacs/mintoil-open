const isDebug = localStorage.getItem('env') === 'debug';
require([
    'require',
    `chrome-extension://{{chrome.runtime.id}}/js/lib/logger${!isDebug ? '.min' : ''}.js`,
    'jquery',
    'underscore'
], (require, Logger) => {
    let $ = require('jquery');
    let _ = require('underscore');

    //#region Logger
    /* This is to make sure we can tell MintOil logs apart from standard console logs. */
    // noinspection JSUnusedLocalSymbols,DuplicatedCode
    Logger.useDefaults({
        formatter: function (messages, context) {
            messages.splice(0, 0, `%c{{MINTOIL_CONSTANTS.application.name}}-Injected`);
            messages.splice(1, 0, `{{MINTOIL_CONSTANTS.logger.style}}`);
        }
    });

    // We don't really need logging if the environment isn't set for it.
    const logger = Logger.get('Injected');
    if (!isDebug) {
        logger.setLevel(Logger.ERROR);
    } else {
        logger.setLevel(Logger.TRACE);
    }
    //#endregion

    $('#mintoilOverview').on("click", () => {
        window.open("https://mintoil.caelusminds.com", "_blank", null);
    });


    setTimeout(() => {
        fetch(`https://api.bitbucket.org/2.0/repositories/caelusminds/mintoil-open/issues?q=version.name%3D%22{{chrome.runtime.version_name}}%22+AND+state%3D%22resolved%22&fields=-values.assignee,-values.reporter,-values.watches`)
            .then((response) => {
                return response.json();
            })
            .then((_issues) => {
                let issues = _issues.values;
                let list = [];
                let grouping;
                let groupingKeys;

                issues.forEach(async (issue) => {
                    let url = `${issue.links.self.href}/changes?q=changes.state.new%3D%22resolved%22&fields=-values.issue,-values.links,-values.user`;
                    await fetch(url).then(response => response.json()).then((data) => {
                        issue.changes = data.values;
                    });
                });

                grouping = _.groupBy(issues, (issue) => issue.component.name);
                groupingKeys = Object.keys(grouping);
                groupingKeys.forEach((group_name) => {
                    list.push(`<label class="list-section">${group_name.replace(/->/gi, `&rarr;`)}</label>`);
                    grouping[group_name].forEach(issue => {
                        list.push(`<li class="${issue.kind}">${issue.title}${issue.content.html}</li>`);
                    });
                });

                $("#this-version-wrapper").html(list.join("\r\n"));
            });
    }, 2000);

});
