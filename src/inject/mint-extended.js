// noinspection JSUnresolvedVariable
window.MintOil = {
    id: '{{chrome.runtime.id}}',
    datePickerReloaded: false,
    Bills: [],
    Providers: [],
    Receipts: [],
    Events: {},
    NavTo: () => {
        window.location.pathname = '/mintoil.event';
    },
    /*    ForeverSession: () => {
            require(['models/ApplicationModel', `chrome-extension://{{chrome.runtime.id}}/js/lib/localforage.min.js`,],
                (
                    /!**module:models/ApplicationModel*!/ ApplicationModel,
                    /!**LocalForage*!/ localForage,
                ) => {
                    localForage.getItem("keep-session").then((d) => {
                        ApplicationModel.getInstance().resetIdleTimer();
                        setTimeout(window.MintOil.ForeverSession, 30000);
                    });
                });
        },*/
    /*CheckOfferTimeout: null,
    CheckForOffers: (retryCount) => {
        require(['jquery'], ($) => {
            // Listen to MINT Ads being dismissed...
            let $offers = $('.promotions-personalized-offers-ui');

            if ($offers.length === 0) {
                if (retryCount < 10) {
                    console.log("What's the count?", {retryCount: retryCount, timeout: 100 * retryCount});
                    window.MintOil.CheckOfferTimeout = setTimeout(
                        window.MintOil.CheckForOffers,
                        100 * retryCount,
                        retryCount++
                    );
                }
            } else {
                setTimeout(() => {
                    $(".dismissOffer").on('click', function (e) {
                        console.log("Advert Closed", {event: e, this: this});
                    });
                }, 500);
            }
        });
    },*/
    CompileConstants: {
        'chrome': {
            "runtime": {
                "version_name": `{{chrome.runtime.getManifest().version_name}}`,
                "version": `{{chrome.runtime.getManifest().version}}`,
                "id": `{{chrome.runtime.id}}`
            },
        },
        'MINTOIL_CONSTANTS': {
            'application': {
                'name': `{{MINTOIL_CONSTANTS.application.name}}`
            },
            'logger': {
                'style': `{{MINTOIL_CONSTANTS.logger.style}}`
            },
            'licensing': {
                'type': '{{lic_type}}',
                'days_left': ((dl) => {
                    // noinspection EqualityComparisonWithCoercionJS
                    if(dl.toLowerCase().indexOf('null') >= 0 || dl.length === 0){
                        return 0;
                    }
                    return dl;
                })(`{{days_left}}`),
                'days_left_style': ((dl) => {
                    // noinspection EqualityComparisonWithCoercionJS
                    if(dl.toLowerCase().indexOf('null') >= 0 || dl.length === 0 ){
                        return `style="visibility:hidden;"`;
                    }
                    return ``;
                })(`{{days_left}}`)
            }
        },
        'lang': {
            'features': {
                'monthly_calc': "Monthly Calculator"
            },
            'bill-name': `Bill Name`,
            'auto-pay': `Auto-pay`,
            'payments': {
                'full': `Payment Due (Full)`,
                'min': `Payment Due (Min)`,
                'paid': `Amount Paid`,
                'ideal': `Ideal Payment`,
                'rec': `Calculated Payment`
            },
            'exports': {
                'is': {
                    'autopaid': `Is Autopaid?`,
                    'paid': `Is Paid?`,
                    'paid-in-full': `Is Paid in Full?`,
                    'fully-scheduled': `Is Payment Scheduled?`
                },
                'scheduled': `Scheduled Payment`,
                'notes': `Notes`
            },
            'date-due': `Date Due`
        }
    }
};

try {
    require(['backbone', 'handlebars', 'jquery'],
        (backbone, handlebars, /**JQueryStatic*/$) => {

            let onNavClick = () => {
                $('.mintoilPage').hide();
                $('.productPageContent div[class$=Page]:not([class=mintoilPage])').show();
            };

            // Whenever something OTHER than a MintOil nav-item is clicked, hide MintOil and show the other page...
            $('.navbar-nav li:not("#mintoil"):not("#mintoil-footer") a').on('click', onNavClick);
            $(".navbar-right .right li").on("click", onNavClick);
            $("#logo-navbar > a#logo-link").on("click", onNavClick);

            // Add our own listen to the navigate events in Mint's Backbone events.
            backbone.listenTo(backbone.Events, "navigate", (page) => {
                // Run this regardless of the page...
                //if (window.MintOil.CheckOfferTimeout !== null) clearTimeout(window.MintOil.CheckOfferTimeout);
                //window.MintOil.CheckForOffers(1);

                // Only run this if it is the bills page....
                if (page.indexOf('/bills.event') >= 0) {
                    // All this recursivity is due the fact the user can click the menu option, which will give
                    //   the DOM checks a false positive, and refresh.
                    let run = () => {
                        window.MintOil.GenerateCalendarButtons();
                        let runCheck = () => {
                        };
                        (runCheck = (/**JQueryStatic*/$) => {
                            if ($('#tab-list').length !== 1) {
                                setTimeout(runCheck, 500, $);
                            } else {
                                window.MintOil.AddBillPaymentCalculator();
                            }
                        })($);
                    };

                    // If we're already on the page, lets give it a half-second to load, and try again.
                    if (document.location.pathname === '/bills.event') {
                        setTimeout(run, 1000);
                    } else {
                        run();
                    }
                }

                if (page.indexOf('/transaction.event') >= 0) {
                    (function tryAgain() {
                        let $search = $('#search-input');
                        if ($search.length === 0) {
                            setTimeout(tryAgain, 500);
                        } else {
                            window.MintOil.LoadTransactionPage();
                        }
                    })();
                }
            });

            //window.MintOil.CheckForOffers(1);


        });

    window.MintOil.NavTo =
        /**
         * Override to the inject MintOil.NavTo() function.
         * Injects some triggered events, as well as some style changes that should correspond.
         * @function window.MintOil.NavTo
         */
            () => {
            try {
                console.log("Navigating to MintOil...");
                require(
                    [
                        'mintUtils',
                        'backbone',
                        'handlebars',
                        'jquery',
                        "text!{{f:custom-page.html}}",
                        "text!{{f:custom-page.css}}",
                        "text!{{f:custom-page.js}}",
                        "text!{{f:font-awesome.css}}"
                    ],
                    function (
                        utils,
                        backbone,
                        handlebars,
                        /**JQueryStatic*/ $,
                        /**string*/ template,
                        /**string*/ templateCSS,
                        /**string*/ templateJS,
                        /**string*/ fontAwesomeCSS
                    ) {
                        backbone.Events.trigger("navigate", "/mintoil.event", null, !0);
                        $('title').text("Mint > MintOil");
                        $('ul.navbar-nav li a, ul.right li a.navbar-link').removeClass('selected');
                        $('#mintoil a').addClass('selected');
                        $('#mintoil-footer a').addClass('selected');
                        $('.productPageContent div[class$=Page]:not([class=mintoilPage])').hide();
                        $('.HypothesisTestingView').remove();
                        $('.layer').addClass('hide');
                        let $page = $('.mintoilPage');
                        $page.show();
                        $page.children().show();

                        if (!$page.length) {
                            $('.productPageContent .pageContents')
                                .append(handlebars.compile(template)(window.MintOil.CompileConstants))
                                .append(handlebars.compile(`<style>${templateCSS}</style>`)(window.MintOil.CompileConstants))
                                .append(handlebars.compile(`<style>${fontAwesomeCSS}</style>`)())
                                .append(handlebars.compile(`<script type="text/javascript">${templateJS}</script>`)(window.MintOil.CompileConstants));
                        }
                        utils.scrollToTop();
                    });
            } catch (ex) {
                console.error(ex);
            }
        };

    window.MintOil.Events.onBillCalcClickInverse =
        /**
         * Click Event for handling the NON "Bill Calc" bill page sub-nav items
         * @function window.MintOil.Events.onBillCalcClickInverse
         * @param {Event} e jQuery Event type.
         */
            (e) => {
            require(['jquery'], (/**JQueryStatic*/$) => {
                let leftColClassList = $(".mintbillsLeftCol")[0].classList;
                leftColClassList.forEach(_class => {
                    let newClass = _class.replace(/([\w\-]+)-\d\d/gi, '$1-17');
                    leftColClassList.replace(_class, newClass);
                });

                let rightCol = $(".mintbillsRightCol");

                if (!$(rightCol).is(":visible")) {
                    $(rightCol).show();

                    $('#bill-tac-calc_section').hide();

                    $('.section-wrapper .list').show();
                    $('.add-bill').show();

                    $(e.currentTarget).addClass('selected').removeClass('not-selected');
                    $('#bill-tac-calc').removeClass('selected').addClass('not-selected');
                    $("#bill-tac-calc_month_picker .ui-datepicker").detach('.ui-datepicker').prependTo('#leftColCal');

                    $.datepicker._adjustDate('#leftColCal', 0, "M");
                }

            });
        };

    window.MintOil.Events.onBillCalcClick =
        /**
         * Click Event for handling the "Bill Calc" Bill page sub-navigation item.
         * @function window.MintOil.Events.onBillCalcClick
         * @param {Event} e jQuery Event type.
         */
            (e) => {
            require(['jquery'], (/**JQueryStatic*/$) => {

                let $monthlyBillsTable = $("#monthlyBillsTable");
                if ($monthlyBillsTable.length > 0) {
                    $monthlyBillsTable.dataTable().api().ajax.reload();
                }

                $('#tab-list .selected')
                    .removeClass('selected')
                    .addClass('not-selected');

                $('#bill-tac-calc')
                    .removeClass('not-selected')
                    .addClass('selected');

                $('#bill-tac-calc_section').show();

                $('.section-wrapper .list').hide();
                $('.add-bill').hide();

                let leftColClassList = $(".mintbillsLeftCol")[0].classList;
                leftColClassList.forEach(_class => {
                    let newClass = _class.replace(/([\w\-]+)-\d\d/gi, '$1-24');
                    leftColClassList.replace(_class, newClass);
                });
                $(".mintbillsRightCol").hide();

                $('#leftColCal .ui-datepicker').detach('.ui-datepicker').appendTo("#bill-tac-calc_month_picker");
            });
        };

    window.MintOil.LoadTransactionPage = () => {
        require([
            'jquery',
            'handlebars',
            'text!{{f:date-searches.html}}',
            'text!{{f:date-searches.js}}',
            'text!{{f:date-searches.css}}'
        ], ($, handlebars, $html, $js, $css) => {
            let $search_form = $("#search-form");
            let $date_wrapper = $("#search-date-filter");
            let $fieldset = $search_form.find("fieldset");

            if ($date_wrapper.length === 0) {
                $fieldset
                    .append(handlebars.compile(`<style>${$css}</style>`)(window.MintOil.CompileConstants))
                    .append(handlebars.compile(`${$html}`)(window.MintOil.CompileConstants))
                    .append(handlebars.compile(`<script>${$js}</script>`)(window.MintOil.CompileConstants))
                ;
            }
        });

    };

    window.MintOil.AddBillPaymentCalculator =
        /**
         * Add Bill Payment Calculator
         * @function window.MintOil.AddBillPaymentCalculator
         */
            () => {
            require(
                [
                    'handlebars',
                    'jquery',
                    'text!{{f:monthly-calc.html}}',
                    'text!{{f:monthly-calc.css}}',
                    'text!{{f:monthly-calc.js}}'
                ],
                (
                    handlebars,
                    /**JQueryStatic*/ $,
                    template,
                    templateCSS,
                    templateJS
                ) => {
                    if ($('#tab-list #bill-tac-calc').length > 0) return;

                    $('.tabs #tab-list').css('width', '500px')
                        .append(`<span id="separator"></span>`)
                        .append(`<span class="not-selected" id="bill-tac-calc">${window.MintOil.CompileConstants.lang.features.monthly_calc}</span>`);

                    $('.section-wrapper').append(handlebars.compile(template)(window.MintOil.CompileConstants)
                        .replace(/href=["|'](\/[^\/])/gi, `href="chrome-extension://{{chrome.runtime.id}}$1`))
                        .append(handlebars.compile(`<style>${templateCSS}</style>`)(window.MintOil.CompileConstants))
                        .append(handlebars.compile(`<script type="text/javascript">${templateJS}</script>`)(window.MintOil.CompileConstants));

                    $('#bill-tac-calc').on('click', window.MintOil.Events.onBillCalcClick);
                    $('#tab-list span:not(.separator):not(#bill-tac-calc)').on('click', window.MintOil.Events.onBillCalcClickInverse);
                });
        };

    window.MintOil.GenerateCalendarButtons =
        /**
         * Generates the calendar buttons
         * @function window.MintOil.GenerateCalendarButtons
         */
            () => {
            const CAL_HEADER = 'BEGIN:VCALENDAR\nVERSION:2.0\nPRODID:-//MintOil v0.1a//MintOil Bills Export//EN\nCALSCALE:GREGORIAN';
            const CAL_FOOTER = 'END:VCALENDAR';
            let CAL_BODY = [];

            require(["models/bps/Bills", "models/appservice/Providers"], (billsModel, providers) => {
                let bills = [];
                let exportLink = document.createElement('a');
                let billsInstance = billsModel.getInstance();
                let providersInstance = providers.getInstance();
                providersInstance.fetch().then(() => {
                    let billsByProvider = providersInstance.getAllProviderAccounts({filterClosedAccounts: true})
                        .filter(account => account.get("accountId_BPS"))
                        .map(account => {
                            account.billsref = billsInstance.get({id: account.get("accountId_BPS")});
                            return account;
                        });


                    bills = billsByProvider.map((provider) => {
                        let bill = provider.billsref;
                        return {title: bill.getTitle(), properties: bill.attributes}
                    }).filter((bill) => {
                        return bill.properties.isVisible
                            && (bill.properties.dueDate
                                && bill.properties.dueDate
                                    .replace(`${(new Date()).getFullYear()}-`, "").indexOf((0 + ((new Date()).getMonth() + 1).toString()).slice(-2)) >= 0);
                    });

                    bills.forEach((_) => {
                        if (!_.properties.dueDate || _.properties.dueAmount === 0) return;
                        let timestamp = ((_) => {
                            let s = new Date(_.properties.dueDate);
                            s.setTime(s.getTime());
                            return s.toISOString().replace(/[-:.]/gm, "").replace('T000000000Z', 'T000000Z');
                        })(_);

                        let cal_event = `BEGIN:VEVENT
UID:${_.properties.id}@mint.intuit.com
SUMMARY: ${_.properties.model.toString().replace("_", " ")} DUE | ${_.title}
DTSTAMP:${timestamp}
DTSTART:${timestamp}
DTEND:${timestamp}
DESCRIPTION: Your ${_.title} ${_.properties.model.toString().replace("_", " ").toLowerCase()} is due.
STATUS:CONFIRMED
BEGIN:VALARM
TRIGGER:-PT1440M
DESCRIPTION:Bill Reminder
ACTION:DISPLAY
END:VALARM
END:VEVENT`;
                        CAL_BODY.push(cal_event);
                    });

                    exportLink.setAttribute("id", "current-month-export");
                    exportLink.setAttribute('href', 'data:text/calendar;base64,' + window.btoa((CAL_HEADER + '\n' + CAL_BODY.join('\n') + '\n' + CAL_FOOTER).replace(/\n/gm, '\r\n')));
                    exportLink.setAttribute('download', "mintoil-bills-export.ics");
                    exportLink.appendChild(document.createTextNode('Export Current Bills (.ics)'));

                    let htmlCheck = () => {
                    };
                    // noinspection JSUnusedLocalSymbols
                    (htmlCheck = (exportLink, $) => {
                        let sidebar = document.getElementsByClassName('sidebarWidgets')[0],
                            calWidget = null;

                        if (sidebar) {
                            calWidget = sidebar.getElementsByClassName('CalendarView')[0];
                            sidebar.insertBefore(exportLink, calWidget.nextSibling);
                        } else {
                            setTimeout(htmlCheck, 500, exportLink);
                        }
                    })(exportLink, $);


                });
            });
        };
} catch (ex) {
    console.info("%cIgnoring and handling the exception, that was created by the lack of AMD existing on this page.", "color: #dbdbdb;font-style:italic;");
}
