/**
 * @function
 * @name MintInjection Self-invoking Mint-Injection module.
 * @param {Window.chrome} Chrome API reference.
 */
(function (chrome) {

    /**
     * Default environment reference.
     * @type {string}
     */
    const env = localStorage.getItem('env');

    //#region Logging
    window.Logger.useDefaults({
        formatter: function (messages) {
            messages.splice(0, 0, `%c${MINTOIL_CONSTANTS.application.name}-Injecting`);
            messages.splice(1, 0, `${MINTOIL_CONSTANTS.logger.style}`);
        }
    });

    /**
     * Default logger.
     * @type {ILogger}
     * @const
     */
    const logger = window.Logger.get('Injecting');

    // Set default level
    env !== "debug" ? logger.setLevel(window.Logger.ERROR) : logger.setLevel(window.Logger.TRACE);
    //#endregion


    /**
     * Package Files
     * @type {string[]}
     */
    let package_files = [];

    /**
     * Has injected flag.
     * @type {boolean}
     * @private
     */
    let _injected = false;

    /**
     * Gets the package files.
     * @function
     */
    let getPackageFiles = () => {
        chrome.runtime.sendMessage(
            chrome.runtime.id,
            {command: "getPackageFiles"},
            data => package_files = data
        );
    };
    getPackageFiles();

    /**
     * @class MintOilSendMessage
     * @property {string} command The Command to execute.
     * @property {string} message The message to send with the command.
     */
    class MintOilSendMessage {
        constructor(command, message) {
            this.command = command || null;
            this.message = message || null;
        }
    }

    /**
     * Main injection class.
     * @class MintInjection
     */
    class MintInjection {

        /**
         * Gets injection flag value.
         * @return {boolean}
         */
        static get injected() {
            return _injected;
        }

        /**
         * Sets injection flag value.
         * @param {boolean} value
         */
        static set injected(value) {
            _injected = value;
        }

        /**
         * Gets the SendMessage() functions to be injected into the web-pages javascript.
         * @return {{injectScriptFile: injectScriptFile, quickRunScript: quickRunScript}}
         */
        static get functions() {
            // noinspection JSUnusedGlobalSymbols
            return {
                injectScriptFile: (file, node) => {
                    let mainNode = document.getElementsByTagName(node)[0];
                    let scriptNode = document.createElement('script');
                    scriptNode.setAttribute('type', 'text/javascript');
                    scriptNode.setAttribute('src', file);
                    mainNode.appendChild(scriptNode);
                },
                quickRunScript: (source) => {
                    let script = document.createElement('script');
                    script.setAttribute("id", 'quick-run-' + Math.floor(Math.random() * 10000 * 1000));
                    script.type = "text/javascript";
                    script.innerHTML = `try{${source}}catch(ex){console.error("%c${MINTOIL_CONSTANTS.application.name}", "${MINTOIL_CONSTANTS.logger.style}", ex);}`;
                    document.body.appendChild(script);
                    setTimeout(() => {
                        document.body.removeChild(script)
                    }, 1000);
                }
            }
        }

        /**
         * Gets various contextual Event Listeners.
         * @return {{mintOilClicked: mintOilClicked, onMessage: onMessage, onDocumentLoad: onDocumentLoad}}
         */
        static get listeners() {
            // noinspection JSUnusedLocalSymbols
            return {
                /**
                 * On chrome.runtime.OnMessage()
                 * @param {MintOilSendMessage|string|*} message The message sent by the calling script.
                 * @param {chrome.runtime.MessageSender} sender The message sender.
                 * @param {function} sendResponse Function to call when there is a response. Return Object, will jsonify.
                 */
                onMessage: (message, sender, sendResponse) => {
                    /**
                     * Run check stub.
                     * @function
                     */
                    let runCheck = () => {
                    };

                    switch (message.command) {
                        case 'navto-mintoil':
                            (runCheck = () => {
                                if (!MintInjection.injected) {
                                    setTimeout(runCheck, 100);
                                } else {
                                    MintInjection.listeners.mintOilClicked();
                                }
                            })();
                            break;
                        case 'show-ical-button':
                            (runCheck = () => {
                                if (!MintInjection.injected) {
                                    setTimeout(runCheck, 100);
                                } else {
                                    setTimeout(() => {
                                        MintInjection.functions.quickRunScript(
                                            `MintOil.GenerateCalendarButtons();
                                            window.MintOil.AddBillPaymentCalculator();`
                                        );
                                    }, 1000);
                                }
                            })();
                            break;
                        case 'load-transaction-page':
                            (runCheck = () => {
                                if (!MintInjection.injected) {
                                    setTimeout(runCheck, 100);
                                } else {
                                    setTimeout(() => {
                                        MintInjection.functions.quickRunScript(`window.MintOil.LoadTransactionPage();`);
                                    }, 1000);
                                }
                            })();
                            break;
                    }
                },
                /**
                 * MintOil nav item click event.
                 * @param {Event} [event] Optional element event.
                 */
                mintOilClicked: (event) => {
                    logger.log(event);

                    if (event) {
                        event.preventDefault();
                        event.stopPropagation();
                    }

                    try {
                        let mintOilScriptId = "mint-oil--set-page";
                        let scriptInjectElement = document.getElementById(mintOilScriptId);
                        if (scriptInjectElement) {
                            scriptInjectElement.remove();
                        }
                        let script = document.createElement('script');
                        script.setAttribute("id", mintOilScriptId);
                        script.type = "text/javascript";
                        script.innerHTML = `try{window.MintOil.NavTo();}catch(ex){console.error("%c${MINTOIL_CONSTANTS.application.name}", "${MINTOIL_CONSTANTS.logger.style}", ex);}`;
                        document.body.appendChild(script);
                        setTimeout(() => {
                            try {
                                document.body.removeChild(script)
                            } catch (ex) {
                                logger.debug("Catching Node Exception", ex);
                            }
                        }, 5000);
                    } catch (ex) {
                        logger.error(ex);
                    }
                },
                onDocumentLoad: (retry) => {
                    let timeoutInSeconds = 0.5;
                    if (isNaN(retry)) {
                        retry = 0;
                    }
                    retry = (!retry) ? 1 : retry;

                    logger.info('The DOM on MINT has loaded, looking for the navigation...', retry);

                    let navBars = document.getElementById("tabs-production");
                    if (!navBars) {
                        logger.info(`Nope, nothing yet, trying again in ${timeoutInSeconds} seconds...`);
                        setTimeout(function () {
                            MintInjection.listeners.onDocumentLoad(++retry);
                        }, timeoutInSeconds * 1000);
                        return;
                    }

                    let footNav = document.getElementsByClassName("navbar-nav")[1];

                    logger.info("Found some nav bars, lol", navBars);

                    let mintOilNavID = "mintoil";
                    let mintOilNav = document.createElement("li");
                    let mintOilNavAnchor = document.createElement("a");

                    mintOilNav.setAttribute("id", mintOilNavID);
                    mintOilNav.setAttribute("class", "col");
                    mintOilNavAnchor.innerHTML = chrome.runtime.getManifest().short_name;
                    mintOilNavAnchor.setAttribute("href", "/mintoil.event");
                    mintOilNav.appendChild(mintOilNavAnchor);
                    navBars.appendChild(mintOilNav);
                    // noinspection JSUnresolvedFunction
                    document.getElementById(mintOilNavID).addEventListener("click", MintInjection.listeners.mintOilClicked);

                    let mintOilFootNavID = "mintoil-footer";
                    if (footNav) {
                        let mintOilFootNav = document.createElement("li");
                        let mintOilFootNavAnchor = document.createElement("a");

                        mintOilFootNav.setAttribute("id", mintOilFootNavID);
                        mintOilFootNav.setAttribute("class", "col");
                        mintOilFootNavAnchor.innerHTML = chrome.runtime.getManifest().short_name;
                        mintOilFootNavAnchor.setAttribute("href", "/mintoil.event");
                        mintOilFootNav.appendChild(mintOilFootNavAnchor);
                        footNav.appendChild(mintOilFootNav);
                        // noinspection JSUnresolvedFunction
                        document.getElementById(mintOilFootNavID).addEventListener("click", MintInjection.listeners.mintOilClicked);
                    } else {
                        let oldMintFooters = document.getElementById('body-footer').getElementsByClassName('main-pages')[0];
                        let mintOilFootNavAnchor = document.createElement("a");
                        document.getElementById("ftr-save").setAttribute("class", "");
                        mintOilFootNavAnchor.innerHTML = chrome.runtime.getManifest().short_name;
                        mintOilFootNavAnchor.setAttribute("id", mintOilFootNavID);
                        mintOilFootNavAnchor.setAttribute("class", "last");
                        mintOilFootNavAnchor.setAttribute("href", "/mintoil.event");
                        oldMintFooters.appendChild(mintOilFootNavAnchor);
                        // noinspection JSUnresolvedFunction
                        document.getElementById(mintOilFootNavID).addEventListener("click", MintInjection.listeners.mintOilClicked);
                    }

                    let supportedLocations = window.localStorage.getItem('supportedLocations');
                    supportedLocations = supportedLocations.replace(/\|bills\|/, '|bills|mintoil|');
                    window.localStorage.setItem('supportedLocations', supportedLocations);


                    try {
                        let mintExtendedFilename = env ? 'src/inject/mint-extended.js' : 'src/inject/mint-extended.min.js';
                        let getJs = fetch(chrome.runtime.getURL(mintExtendedFilename));
                        getJs
                            .then(response => response.text())
                            .then(async content => {
                                logger.debug("Packages Files", package_files);

                                let mintOilScriptId = "mint-oil--set-page";
                                let script = document.createElement('script');

                                // This should now handle ALL file replacements. They simply need to be formatted like {{f:name_of_file.html}}.
                                // noinspection RegExpRedundantEscape
                                content = content.replace(/\{\{f:([^\}]+)\}\}/gim, (foundString) => {
                                    let file = package_files[foundString.replace("{{f:", "").replace("}}", "")];
                                    logger.debug("Package File Regex", foundString, file);
                                    return file.extensionPath || `""`;
                                });

                                // noinspection RegExpRedundantEscape
                                content = content.replace(/\{\{([^\}]+)\}\}/gim, (foundString) => {
                                    let retVal = "";
                                    foundString = foundString.replace("{{", "").replace("}}", "");
                                    try {
                                        retVal = foundString.split(".").reduce((obj, i) => i.indexOf("()") >= 0 ? obj[i.replace('()', '')]() : obj[i], window);
                                    } catch (ex) {
                                        logger.warn("Trying to use a replacer that doesn't exist.", ex);
                                    }
                                    return retVal;
                                });

                                script.setAttribute("id", mintOilScriptId);
                                script.type = "text/javascript";
                                script.innerHTML = `${content}`;
                                document.body.appendChild(script);
                                MintInjection.injected = true;

                                document.body.addEventListener('click', function (e) {
                                }, {passive: true});
                            });
                    } catch (ex) {
                        logger.error(ex);
                    }
                }
            };
        };
    }


    chrome.runtime.onMessage.addListener(MintInjection.listeners.onMessage);
    document.addEventListener("DOMContentLoaded", () => {
        MintInjection.listeners.onDocumentLoad();
    });

})(chrome);