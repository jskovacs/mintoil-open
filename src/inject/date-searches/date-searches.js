const isDebug = localStorage.getItem('env') === 'debug';
require([
        'jquery',
        'underscore',
        'moment',
    ].concat([
        `chrome-extension://{{chrome.runtime.id}}/js/lib/logger${!isDebug ? '.min' : ''}.js`,
        'vendors/jquery/jquery.ui.core',
        'vendors/jquery/jquery.ui.datepicker'
    ]),
    ($, _, moment, Logger) => {
        const dateFormat = 'mm/dd/yy';
        const prefix = "#location:";
        let codeRun = false;

        //#region Logger
        /* This is to make sure we can tell MintOil logs apart from standard console logs. */
        // noinspection JSUnusedLocalSymbols
        Logger.useDefaults({
            formatter: function (messages, context) {
                messages.splice(0, 0, `%c{{MINTOIL_CONSTANTS.application.name}}-Injected`);
                messages.splice(1, 0, `{{MINTOIL_CONSTANTS.logger.style}}`);
            }
        });

        // We don't really need logging if the environment isn't set for it.
        const logger = Logger.get('Injected');
        if (!isDebug) {
            logger.setLevel(Logger.ERROR);
        } else {
            logger.setLevel(Logger.TRACE);
        }
        //#endregion

        let $from = $('#search-date-start');
        let $to = $('#search-date-end');
        let $search_form = $("#search-form");
        let $search_input = $("#search-input");
        /**
         * Gets the HASH from the URL
         * @returns {string}
         */
        let getHash = () => {
            return window.location.hash;
        };
        let getHashQuery = () => {
            return getHash() ? JSON.parse(decodeURIComponent(getHash().replace(prefix, ""))) : {}
        };
        let commonProps = {
            prevText: '',
            nextText: '',
            dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
            monthNamesShort: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
            showOtherMonths: true,
            selectOtherMonths: true,
            changeMonth: true,
            changeYear: true,
            dateFormat: dateFormat,
            autoSize: false,
            defaultDate: "+1w",
            showAnim: 'slideDown',
            showButtonPanel: true,
            currentText: `<span class="cal-today">Today</span>`,
            onSelect: function (date, inst) {
                let $search_date = $('#search-date');
                let $search_filters = $("#search-filters");
                let isSearchFiltersHidden = $search_filters.hasClass('hide');
                let isSearchDateHidden = $search_date.hasClass('hide');
                let dateHidden = isSearchDateHidden || isSearchFiltersHidden;

                logger.debug("Is Date Hidden?", dateHidden);

                if (dateHidden === false) {
                    let evt = new MouseEvent('click', {
                        bubbles: true,
                        cancelable: true,
                        view: window
                    });
                    codeRun = true;
                    $("#search-date .cancel")[0].dispatchEvent(evt);
                }

                if ($from[0].id === inst.id) {
                    $to.datepicker("option", "minDate", getDate($from[0]));
                } else if ($to[0].id === inst.id) {
                    $from.datepicker("option", "maxDate", getDate($to[0]));
                }
            }
        };
        let getDate = (element) => {
            let date;
            try {
                date = $.datepicker.parseDate(dateFormat, element.value);
            } catch (error) {
                date = null;
            }
            return date;
        };

        let eventRaise = function (e) {
            let query = getHashQuery();
            /**
             * From Date
             * @type {Date|null}
             */
            let fromDate = $from.datepicker('getDate');
            /**
             * To Date
             * @type {Date|null}
             */
            let toDate = $to.datepicker('getDate');

            if (fromDate !== null) {
                query.dateStart = moment(fromDate).format('MM/DD/YYYY');
            }

            if (fromDate !== null) {
                query.dateEnd = moment(toDate).format('MM/DD/YYYY');
            }

            logger.debug("Refresh the HASH :: BEFORE ::", getHash());

            setTimeout(() => {
                let nq = getHashQuery();
                query.cacheBust = Date.now();

                if (nq.query.length > 0) query.query = nq.query;
                if (!query.hasOwnProperty("query")) query.query = "";
                if (!query.hasOwnProperty("offset")) query.offset = 0;
                if (!query.hasOwnProperty("typeFilter")) query.typeFilter = "cash";
                let newHash = `${prefix}${encodeURIComponent(JSON.stringify(query, (k, v) => v === undefined ? null : v)).replace(/"/g, '%22')}`;
                let oldHash = getHash();
                window.document.location.hash = window.document.location.hash.replace(oldHash, newHash);
                logger.debug("Refresh the HASH :: AFTER ::", document.location.hash);
            }, !isNaN(e) ? e : 1500);

        };
        let runRetry = (e) => {
            (function retry() {
                let $search_date = $('#search-date');
                let $search_filters = $("#search-filters");
                let isSearchFiltersHidden = $search_filters.hasClass('hide');
                let isSearchDateHidden = $search_date.hasClass('hide');
                let dateHidden = isSearchDateHidden || isSearchFiltersHidden;

                if ($search_date.length > 0) {
                    logger.debug("Lets try this... date change");
                    if ($to.val().length > 0 || $from.val().length > 0) {
                        logger.debug("Lets try this... date change:: Raise Event");
                        eventRaise(e);
                    }
                } else {
                    logger.debug("Lets try one more time...");
                    setTimeout(retry, 100);
                }
            })();
        };

        $from.datepicker(commonProps);
        $to.datepicker(commonProps);

        if (getHashQuery().hasOwnProperty("dateStart")) {
            logger.debug("START DATE", getHashQuery().dateStart);
            $from.val(getHashQuery().dateStart);
        }

        if (getHashQuery().hasOwnProperty("dateEnd")) {
            logger.debug("END DATE", getHashQuery().dateEnd);
            $to.val(getHashQuery().dateEnd);
        }

        if ($to.val().length > 0 || $from.val().length > 0) {
            setTimeout(runRetry, 1000, 100);
        }


        $search_form.on("submit", (e) => runRetry(e));

        $search_input.on('keydown', (e) => {
            if (e.which === 13) {
                runRetry();
            }
        });

        $('#search-date a.cancel,#search-clear a.cancel').on('click', function (e) {
            logger.debug("Mouse Event on Search Date Cancel", {codeRun, e});
            if (!codeRun) {
                $(".search-form-date").val(null);
            }
            codeRun = false;
        });

    });
