let that = {
    _id: undefined,
    _month_group: undefined,
    _custom_input: undefined
};

/**
 * @interface IBillingCalculatorEntry
 * @property {string}   id              Unique(ish) identifier for the calculator entry.
 * @property {number}   month_group     Group that the calc-entry belongs to.
 * @property {number}   custom_input    The user's custom input.
 */

/**
 * Encapsulates the members and methods of an entry in the BillingCalculator namespace.
 * @class BillingCalculatorEntry
 * @public
 * @implements IBillingCalculatorEntry
 */
class BillingCalculatorEntry {

    get id() {
        return that._id;
    }

    set id($_) {
        that._id = this._onPropertyChanged("id", $_);
    }

    get month_group() {
        return that._month_group;
    }

    set month_group($_) {
        that._month_group = this._onPropertyChanged("month_group", $_);
    }

    get custom_input() {
        return that._custom_input;
    }

    set custom_input($_) {
        that._custom_input = this._onPropertyChanged("custom_input", $_);
    }

    /**
     * Default constructor, designed to create a basic rep of this object.
     * @param {string}      [id]            Unique(ish) identifier for the calculator entry.
     * @param {number}      [month_group]   Group that the calc-entry belongs to.
     * @param {number}      [custom_input]  The user's custom input.
     */
    constructor(id, month_group, custom_input) {
        this.id = id;
        this.month_group = month_group;
        this.custom_input = custom_input;
    }

    /**
     * Creates an entry for the billing calculator.
     * @param {string}      [id]            Unique(ish) identifier for the calculator entry.
     * @param {number}      [month_group]   Group that the calc-entry belongs to.
     * @param {number}      [custom_input]  The user's custom input.
     * @returns {IBillingCalculatorEntry}
     * @function
     * @static
     */
    static create(id, month_group, custom_input){
        return new BillingCalculatorEntry(id, month_group, custom_input);
    }

    /**
     * Event for capturing property changes.
     * @param {string} propertyName
     * @param {*} newValue
     * @return {*}
     * @private
     */
    _onPropertyChanged(propertyName, newValue){
        let oldValue = this[propertyName];
        console.debug("Property Changed", {propertyName, oldValue, newValue});
        return newValue;
    }
}